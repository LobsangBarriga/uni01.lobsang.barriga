/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.modelo.CalculadoraInteres;

/**
 *
 * @author Lobsang Barriga
 */
@WebServlet(urlPatterns = {"/CalculadoraInteresController"})
public class CalculadoraInteresController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CalculadoraInteresController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CalculadoraInteresController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        System.out.println("========== FLAG DEBUG #001 ==========");
        System.out.println("Valor del input numCapital: " + request.getParameter("numCapital"));
        System.out.println("Valor del input numTasa: " + request.getParameter("numTasa"));
        System.out.println("Valor del input numYear: " + request.getParameter("numYear"));
        System.out.println("Valor del botón: " + request.getParameter("calcular"));
        
        double capital = Double.parseDouble(request.getParameter("numCapital").replace(",","."));
        double tasaAnual = Double.parseDouble(request.getParameter("numTasa").replace(",","."));
        double numAnos = Double.parseDouble(request.getParameter("numYear").replace(",","."));
        
        System.out.println("========== FLAG DEBUG #002 ==========");
        CalculadoraInteres calculadora = new CalculadoraInteres();
        calculadora.calcular(capital, tasaAnual, numAnos);
        
        System.out.println("========== FLAG DEBUG #003 ==========");
        request.setAttribute("calculadora", calculadora);
        request.getRequestDispatcher("resultado.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
