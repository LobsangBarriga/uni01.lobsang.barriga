package root.modelo;

// Autor: Lobsang Barriga

public class CalculadoraInteres {
    
    private double capital;
    private double tasaAnual;
    private double numAnos;
    private double resultado;

    // GETTERS Y SETTERS
    
    public double getCapital() {
        return capital;
    }

    public void setCapital(double capital) {
        this.capital = capital;
    }

    public double getTasaAnual() {
        return tasaAnual;
    }
    
    public void setTasaAnual(double tasaAnual) {
        this.tasaAnual = tasaAnual;
    }

    public double getNumAnos() {
        return numAnos;
    }

    public void setNumAnos(double numAnos) {
        this.numAnos = numAnos;
    }
    
    public double getResultado() {
        return resultado;
    }

    public void setResultado(double resultado) {
        this.resultado = resultado;
    }
    
    public String calcular(){
        String resultadoCalculo;
        System.out.println("Entrando a la 2da funcion calcular");
        System.out.println("Capital: " + this.getCapital());
        System.out.println("Tasa Anual: " + this.getTasaAnual());
        System.out.println("Número de años: " + this.getNumAnos());
        System.out.println("Resultado: " + (this.getCapital() * (this.getTasaAnual()/100) * this.getNumAnos()));
        resultadoCalculo = Double.toString(this.getCapital() * (this.getTasaAnual()/100) * this.getNumAnos());
        return "$" + resultadoCalculo.replace(".", ",");
        //return Double.toString(this.getCapital() * (this.getTasaAnual()/100) * this.getNumAnos());
    }
    
    public String calcular(double capital, double tasaAnual, double numAnos){
        //I = C * (i/100) * n; donde I = interés simple producido, C = capital, i = tasa interés anual y n = número de años 
        System.out.println("Entrando a la funcion calcular");
        //this.setResultado(capital * (tasaAnual/100) * numAnos);
        this.setCapital(capital);
        this.setTasaAnual(tasaAnual);
        this.setNumAnos(numAnos);
        return calcular();
    }
    
}
