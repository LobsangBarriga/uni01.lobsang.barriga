<%-- 
    Document   : resultado
    Created on : 08-04-2021, 20:10:33
    Author     : Lobsang Barriga
--%>

<%@page import="root.modelo.CalculadoraInteres"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    CalculadoraInteres cal = (CalculadoraInteres)request.getAttribute("calculadora");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- BOOTSTRAP CDN -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <style>
            body{
                background: linear-gradient(90deg, #EDF46C, #56C75A);
                font-family: sans-serif;
            }
            #divPrincipal{
                background-color: white;
                margin-top: 30px;
                padding: 30px;
                border-radius: 10px;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col" id="divPrincipal">
                    <h4 style="text-align: center;">El interés calculado es:</h4>
                    <form name="form" action="CalculadoraInteresController" method="POST">
                        <h1 name="resultado" style="text-align: center;"><%= cal.calcular()%></h1>
                    </form>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </body>
</html>
