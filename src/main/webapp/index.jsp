<%-- 
    Document   : index
    Created on : 08-04-2021, 12:37:48
    Author     : Lobsang Barriga
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- BOOTSTRAP CDN -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <style>
            body{
                background: linear-gradient(90deg, #EDF46C, #56C75A);
                font-family: sans-serif;
            }
            #divPrincipal{
                background-color: white;
                margin-top: 30px;
                padding: 30px;
                border-radius: 10px;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col" id="divPrincipal">
                    <h1 style="text-align: center;">UNI01 - Calculadora intereses</h1>
                    <h6 style="text-align: center; color: #666666;">Autor: Lobsang Barriga (Sección 50)</h6>
                    <hr>
                        <form name="miFormulario" action="CalculadoraInteresController" method="POST">
                            <div class="form-group row">
                                <label for="numCapital" class="col-sm-4 col-form-label">Ingrese Capital:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="numCapital" class="form-control" placeholder="Capital" required></input>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="numTasa" class="col-sm-4 col-form-label">Ingrese Tasa Anual:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="numTasa" class="form-control" placeholder="Tasa Anual" required></input>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="numYear" class="col-sm-4 col-form-label">Número de Años:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="numYear" class="form-control" placeholder="Número de Años" required></input>
                                </div>
                            </div>
                            <hr>
                            <button type="submit" name="calcular" value="miBoton" class="btn btn-primary btn-block">Calcular</button>
                        </form>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </body>
</html>
